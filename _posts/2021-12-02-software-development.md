---
layout: post
cover: 'assets/images/vfd_cover_projects.jpg'
title: Software development
date:   2021-12-02 00:00:00
tags: projects
subclass: 'post tag-test tag-content'
categories: 'vfonsecad'
navigation: True
logo: 'assets/images/vfd_hola.png'
---


**pycaltransfer: Calibration transfer in Python**

Calibration transfer for chemometrics and spectral data applications
This package contains methods to perform calibration transfer based on bilinear models, mainly Partial Least Squares Regression. Numpy and Sci-Kit Learn are mandatory dependencies  

[See pip](https://pypi.org/project/pycaltransfer/)    
[See gitlab repo](https://gitlab.com/chemsoftware/python/pycaltransfer)

**rcaltransfer: Calibration transfer in R**

Calibration transfer for chemometrics and spectral data applications
This package contains methods to perform calibration transfer based on bilinear models, mainly Partial Least Squares Regression. Numpy and Sci-Kit Learn are mandatory dependencies
    
[See gitlab repo](https://gitlab.com/chemsoftware/rproject/rcaltransfer)
