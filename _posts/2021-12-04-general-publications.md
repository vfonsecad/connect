---
layout: post
cover: 'assets/images/vfd_cover_projects.jpg'
title: General publications
date:   2021-12-04 00:00:00
tags: projects
subclass: 'post tag-test tag-content'
categories: 'vfonsecad'
navigation: True
logo: 'assets/images/vfd_hola.png'
---


**The Lifetime of a Machine Learning Model**

When are they born and how do they transform?

[See article](https://medium.com/towards-data-science/the-lifetime-of-a-machine-learning-model-392e1fadf84a)

**Model employment: The inference comes after training, not during**

Training and using models are two separate phases

[See article](https://medium.com/towards-data-science/model-employment-the-inference-comes-after-training-not-during-6129efdf8e90)

**Prediction Performance Drift: The Other Side of the Coin**

We know the causes, let’s talk about the types

[See article](https://medium.com/towards-data-science/prediction-performance-drift-the-other-side-of-the-coin-4bd3c7334b70)

**Is Interpreting ML Models a Dead-End?**

The interpretation process can be detached from the model architecture

[See article](https://towardsdatascience.com/is-interpreting-ml-models-a-dead-end-f5b9dd78ba77)



**GUI’s or coding: Production vs. Operation**

As we enter deeper into the era of data and computer science, a debate for the tools we use comes about software as GUI or coding. Which one should we use after all?

[See article](https://towardsdatascience.com/guis-or-coding-production-vs-operation-fc1de9e483a8)


**On the value of reproducibility**

What does reproducibility represent?

[See article](https://medium.com/towards-data-science/on-the-value-of-reproducibility-ea6907aaa9a3)


**Interpreting the model is for humans, not for computers**

_In co-authorship with [Julian Cruz](https://www.cruzjulian.com/)_

It is about translating, not rephrasing.

[See article](https://towardsdatascience.com/interpreting-the-model-is-for-humans-not-for-computers-9a857011ff3)
