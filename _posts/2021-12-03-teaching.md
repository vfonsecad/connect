---
layout: post
cover: 'assets/images/vfd_cover_projects.jpg'
title: Teaching
date:   2021-12-03 00:00:00
tags: projects
subclass: 'post tag-test tag-content'
categories: 'vfonsecad'
navigation: True
logo: 'assets/images/vfd_hola.png'
---


**Fundamentals of version control using Git**

Workshop dedicated to young researchers who are in need to learn version control    
[See presentation](https://mvgroup.gitlab.io/version_control_tutorial/docs/presentation/)
[See tutorial](https://mvgroup.gitlab.io/version_control_tutorial/docs/tutorial/)
[See material for training](https://gitlab.com/mvgroup/version_control_tutorial)

**Descriptive and Inferential Statistics**
_January 2015 - July 2015_
 Courses for Economists and Engineers at Universidad Los Libertadores in Bogota, Colombia.
 The courses included basic descriptive techniques for data analysis, probability, and univariate and multivariate inferential statistics (probability distributions, hypothesis testing, confidence intervals and linear regression).

**Volunteering teaching experience**

- Fundamental concepts of statistics for master students
- Python programming and project structure for data science students
- Design of Experiments: emphasis on Optimal Designs (D-optimal and I-optimal)
