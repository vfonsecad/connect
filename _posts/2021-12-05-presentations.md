---
layout: post
cover: 'assets/images/vfd_cover_projects.jpg'
title: Presentations
date:   2021-12-05 00:00:00
tags: projects
subclass: 'post tag-test tag-content'
categories: 'vfonsecad'
navigation: True
logo: 'assets/images/vfd_hola.png'
---


**Reproducibility cheat sheet**

Summary infographics of the elements of reproducibility for project development in academy and industry     
[See pdf](https://gitlab.com/vfonsecad/connect/-/raw/main/assets/pdf/repro_cheatsheet.pdf)

![cheat sheet]({{site.baseurl}}/assets/images/repro_cheatsheet.png)




**Diving into the p-value: From the 1900’s until the ML era**

Presented at congress in Bogota, Colombia for ECCI     
[See presentation](https://mvgroup.gitlab.io/understanding_pvalue/presentation/main)

**(Spanish) Conversation about free software**
**Conversatorio: Perspectivas sobre software libre Trayectoria, usos y visión actual**

Presentado en los diálogos con egresados de la Universidad Nacional de Colombia 2021-2     
[Ver video](https://www.youtube.com/watch?v=5NOzig_zf7c&t=1666s)

**(Spanish) About reproducibility**
**Sobre reproducibilidad y repetibilidad en la academia y en la industria**

Presentado en los diálogos con egresados de la Universidad Nacional de Colombia 2021-1     
[Ver dispositivas](https://vfonsecad.github.io/reproducibilidad_es/dialogos_unal_2021.html)
[Ver video](https://www.youtube.com/watch?v=T7v8FW3Ucis&t=1256s)


<!-- 

**Test**




<iframe src="https://vfonsecad.gitlab.io/colors_move/color_trajectories.html" width="100%" height="750" ></iframe>
 -->
