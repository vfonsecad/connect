---
layout: post
cover: 'assets/images/vfd_cover_projects.jpg'
title: Podcast "We live, therefore we learn"
date:   2020-12-04 00:00:00
tags: projects
subclass: 'post tag-test tag-content'
categories: 'vfonsecad'
navigation: True
logo: 'assets/images/vfd_hola.png'
---

_We live, therefore we learn_ is a project that started in 2019 with [Marco](https://marcodallavecchia.github.io/biologistsadventure/) where we discuss and describe our experiences and learnings. You are all kindly invited to listen to our on the main
      platforms such as [Spotify](https://open.spotify.com/show/09fAylwR3CA53AHHifnOyM?si=miO4P-zwRm2Uc2ydCfGwgw) and
      [Google Podcasts](https://podcasts.google.com/?feed=aHR0cHM6Ly9hbmNob3IuZm0vcy9jZTQwNzZjL3BvZGNhc3QvcnNz)


Some episodes


<iframe src="https://anchor.fm/welivethereforewelearn/embed/episodes/Cooking-is-a-waste-of-time----or-is-it-e8jajo" height="102px" width="400px" frameborder="0" scrolling="no"></iframe>
<iframe src="https://anchor.fm/welivethereforewelearn/embed/episodes/Thinking-about-what-we-eat-changed-our-life-e54epv" height="102px" width="400px" frameborder="0" scrolling="no"></iframe>
<iframe src="https://anchor.fm/welivethereforewelearn/embed/episodes/Traveling-and-living-abroad--heres-why-you-should-do-it-e4n9ah" height="102px" width="400px" frameborder="0" scrolling="no"></iframe>
<iframe src="https://anchor.fm/welivethereforewelearn/embed/episodes/Sustainability-is-easy--Try-once--do-it-forever-e4ou9g" height="102px" width="400px" frameborder="0" scrolling="no"></iframe>
<iframe src="https://anchor.fm/welivethereforewelearn/embed/episodes/I-learned-a-new-language--Heres-what-happened-e4o11p" height="102px" width="400px" frameborder="0" scrolling="no"></iframe>
