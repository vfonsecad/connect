---
layout: post
cover: 'assets/images/vfd_cover_projects.jpg'
title: Academic publications
date:   2021-12-01 00:00:00
tags: projects
subclass: 'post tag-test tag-content'
categories: 'vfonsecad'
navigation: True
logo: 'assets/images/vfd_hola.png'
---


**Domain invariant covariate selection (Di-CovSel) for selecting generalized features across domains**


V.F. Diaz, P. Mishra, J.-M. Roger, W. Saeys,
_Chemometrics and Intelligent Laboratory Systems_,(2022), doi: https://doi.org/10.1016/j.chemolab.2022.104499.
[See more](https://doi.org/10.1016/j.chemolab.2022.104499)    
[See gitlab repository](https://gitlab.com/chemsoftware/python/dicovsel)
![demo](https://gitlab.com/chemsoftware/python/dicovsel/-/raw/main/figures/rice_data.png)
![demo](https://gitlab.com/chemsoftware/python/dicovsel/-/raw/main/figures/selected_variables.png)



**Cost-efficient unsupervised sample selection for multivariate calibration**


Fonseca Diaz, Valeria and De Ketelaere, Bart and Aernouts, Ben and Saeys, Wouter,
_Chemometrics and Intelligent Laboratory Systems_, 104352, 2021.    
doi: 10.1016/j.chemolab.2021.104352    
[See more](https://doi.org/10.1016/j.chemolab.2021.104352)    
[See github repository](https://github.com/vfonsecad/unsupervised-sample-selection/)


**Robustness control in bilinear modeling based on maximum correntropy**

Fonseca Diaz, Valeria and De Ketelaere, Bart and Aernouts, Ben and Saeys, Wouter. _Journal of Chemometrics_   34,  4,  e3215,  2020.  
doi: 10.1002/cem.3215    
[See more](https://doi.org/10.1002/cem.3215)  
[See github repository](https://github.com/vfonsecad/multivariate_calibration)





**Methodology for the Quantification of the Radio Spectrum Available for White Spaces in the Conditions of the Republic of Colombia**

Carlos, Gomez and Martha, Villarreal and Valeria, Fonseca. _International Conference on Advanced Engineering Theory and Applications_,   575--585,  2019, Springer, Cham.    
[See more](https://link.springer.com/chapter/10.1007/978-3-030-53021-1_58)




**Use of non-industrial environmental sensors and machine learning techniques in telemetry for indoor air pollution**

Carlos, Gomez and Valeria, Fonseca and Guillermo, Valencia.  _ARPN J. Eng. Appl. Sci_   
13, 2702--2712, 2018.    
[See more](https://www.researchgate.net/profile/Guillermo-Fernando-Valencia-Plata/publication/343542049_USE_OF_NON-INDUSTRIAL_ENVIRONMENTAL_SENSORS_AND_MACHINE_LEARNING_TECHNIQUES_IN_TELEMETRY_FOR_INDOOR_AIR_POLLUTION/links/5f3061af92851cd302eba117/USE-OF-NON-INDUSTRIAL-ENVIRONMENTAL-SENSORS-AND-MACHINE-LEARNING-TECHNIQUES-IN-TELEMETRY-FOR-INDOOR-AIR-POLLUTION.pdf)
