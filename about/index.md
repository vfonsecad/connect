---
layout: page
title: Valeria Fonseca Diaz
class: 'post'
navigation: True
logo: 'assets/images/vfd_hola.png'
current: about
---

#


Doctor in Bioscience Engineering. Specialized in prediction models for chemical quantification based on spectroscopy techniques. This involves building, transferring and maintenance of calibration models. 


### Education

`2018 - 2022`
**KU Leuven, ​Leuven,​ ​Belgium —  Doctor of Philosophy - PhD, Bioscience Engineering**

Thesis title: Cost-efficient strategies for building, transferring, and maintaining multivariate calibration models for spectroscopic sensors


`2015 - 2017`
**KU Leuven, ​Leuven,​ ​Belgium — MSc in Statistics**

Master dissertation "An efficient chemometric methodology for
spectroscopic quality control of liquid detergents". KU Leuven in
collaboration with P&G Belgium. During the master thesis research, I acquired
training in applied decision-making processes based on parametric,
nonparametric prediction models and data visualization for small and
large-scale problems.


`2009 - 2014`
**Universidad Nacional de Colombia, ​Bogota,​ ​Colombia - BSc in Statistics**


General statistical methodology with a mathematical basis. Bachelor
dissertation "Textual analysis using a latent regression model"

`2013/01 - 2013/06`
**Universidad Nacional Autónoma de México, Mexico City, Mexico**

Exchange student





### Experience


`2023/04 - Present`
*Software Competence Center Hagenberg (SCCH), Hagenberg, Austria — Researcher and Senior Data Scientist*

Bridging scientific research with industries. Focus on transfer learning and model maintenance for spectroscopic sensors 

`2022/10 - 2023/02`
*KU Leuven, Geel,Belgium — ​Postdoctoral researcher*

Maintenance of prediction models for milk composition monitoring. Animal and Human Health Engineering (A2H) KU Leuven.


`2018/07 - 2022/10`
*KU Leuven, ​Leuven,Belgium — ​Doctoral student*

Doctoral student under the strategic basic research fellowship by the ​Fonds
Wetenschappelijk Onderzoek​ (FWO). Faculty of Bioscience Engineering
MeBioS Department.

`2021/08 - 2022/01`
*INRAE, Montpellier, France — ​Doctoral student visitor*

Doctoral student visitor to develop unspuervised orthogonalization methods for calibration transfer

`2017/09 - 2018/06`
*Ipsos, ​Ghent,​ ​Belgium —​ Junior data scientist*

`2017/07 - 2017/08 `
*StepUp, ​Leuven, Belgium —​ Data architect*

`2014/10 - 2015/09`
*Centro Nacional de Consultoría, ​Bogota, Colombia​ ​— Statistician*



### Professional interests (1)

Prediction models for highdimensional data, Software developement, Python, R, Project management

### Professional interests (2)

Teaching, Reproducibility, Free software



### Awards

- FWO strategic basic research fellowship 2018 - Present
- Magna Cum Laude MSc in Statistics 2017
- Science@Leuven scholarship 2015-2017
- Honors degree BSc in Statistics 2014
- BSc scholarship by Universidad Nacional de Colombia 2010-2014


### Languages

- Spanish (Native)
- English (Working proficiency)
- Italian (C1)
- French (A1)
